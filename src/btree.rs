pub trait BTree<K, V>
where
    K: Ord,
{
    fn insert(&mut self, key: K, value: V);

    fn get(&self, key: &K) -> Option<V>;

    fn remove(&mut self, key: &K) -> Option<V>;
}
