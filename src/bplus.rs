//! This module provides a naive implementation of B+-Trees.
//!
//! - Each tree has a fixed even number 2*order for the maximum keys in a node.
//! - Each node has at most that many keys, and every node except the root has at least order.
//! - Only leaf nodes hold (key, value) pairs; interior nodes simply hold keys divider values to organize the tree.
//! The naivete here is that the memory layout is not fully optimized to achieve the CPU cache and/or disk read
//! benefits that B-Trees are capable of. Here I am more focused on the functionality.
//! Furthermore, for simplicity of implementation, I assume that keys and values are Copy to ease moving the key-value pairs around; this could be weakened to clone, however.
//! Also, B-Trees can be made to work without the maximum node size being exactly double the min; this pattern just makes the merging and splitting logic simple.
//!
//! B+-Trees support all of the same operations as B-Trees plus efficient iteration over a range of keys.
//! In a B-Tree, iterating ove all of the keys requires going up and down the tree and thus requires parent pointers.
//! B+-Trees maintain all of the data in the leaves, and furthermore each leaf maintains a pointer to the subsequent leaf.

use super::btree::BTree;
use std::cell::RefCell;
use std::collections::VecDeque;
use std::fmt::Display;
use std::rc::Rc;

/// Tree is a B+-Tree of some fixed order.
///
/// It implements the B-Tree common interface using a standard B-Tree setup as described in the module docs.
/// In addition to the BTree interface, `bplus::Tree` also as has `tree.iter()` method returning an iterator over the
/// pairs (K, V) in the tree. For this, the way the data is stored is slightly different than `traditional::Tree`.
#[derive(Debug, Eq, PartialEq)]
pub struct Tree<K, V> {
    // Having a Tree class house the root node as opposed to using the root itself as the tree
    // makes it easier to handle such situations as the root node splitting due to being full, and so on.
    // For a non-root node, those situations are handled by the parent; for the root, there they are handled by the Tree.
    root: NodeCell<K, V>,
    // As mentioned in the module docs, for simplicity we are focused on the case where the max size is exactly double the min size
    order: usize,
}
// Extra implementatoin details:
// nodes split leaning left: 2*order+1 splits into (order+1) and (order)
// corollary: when traversing, if equal, we proceed LEFT
// if removal causes a node to be deficient, we try to pull an element from the right side, then the left side
// then try to merge with the node on the right, then left.

use Node::*;

impl<K, V> Tree<K, V>
where
    K: Ord + Copy,
    V: Copy,
{
    /// Returns an empty tree of the given order.
    pub fn new(order: usize) -> Tree<K, V> {
        Tree {
            root: Rc::new(RefCell::new(Leaf(LeafNode::new()))),
            order,
        }
    }

    /// Returns an iterator which returns the (key, value) pairs in the tree (using Copy).
    /// Constructing the iterator is O(log(n)) - it traverses the tree to the leftmost leaf.
    /// Subsequently, each `it.next()` call is O(1): the iterator either advance within the leaf
    /// or uses the leaf's pointer to jump to the next leaf
    pub fn iter(&self) -> Iter<K, V> {
        let leaf = match &*self.root.borrow() {
            Inner(i) => i.leftmost_leaf(),
            Leaf(_) => self.root.clone(),
        };
        Iter { leaf, idx: 0 }
    }
}

// The `order` parameter could be brought into the Node type to simplify its method signatures (but duplicate the order all over).
// Doesn't really matter either way.
// The leaf variant is really simple and might not need to have its data extracted into a whole type (LeafNode),
// but InnerNode is a bit more complicated and it's more convenient to be able to give it methods.
#[derive(Debug, Eq, PartialEq)]
enum Node<K, V> {
    Inner(InnerNode<K, V>),
    Leaf(LeafNode<K, V>),
}

// We need the shared ownership plus interior mutability for leaf nodes, since each leaf is referenced from both
// its parent inner node and the previous leaf node. In practice, modifications are only ever made through the
// parent's reference, not the previous leaf's reference.
// For interior nodes, we could get away with just putting them in an Rc. Having interior nodes in an Rc
// makes it much more convenient to do the kinds of reference juggling when nodes are spliting or merging
// or shifting elements to fill too-small nodes. This way, for example, we can get mutable references to multiple
// elements of the Vec of children nodes at once (which would otherwise require borring that Vec as mut more than once).
// It might be possible to not use Rc for that, but it makes it a lot more convenient.
// So putting leaf and inner together, we end up putting every Node in an Rc<RefCell<>> and end up with NodeCell
type NodeCell<K, V> = Rc<RefCell<Node<K, V>>>;

#[derive(Debug, Eq, PartialEq)]
struct LeafNode<K, V> {
    // Data is maintained in sorted order by K
    data: Vec<(K, V)>,
    // Next is only ever None for the rightmost leaf in the tree
    next: Option<NodeCell<K, V>>,
}

impl<K, V> LeafNode<K, V>
where
    K: Ord,
{
    fn new() -> LeafNode<K, V> {
        LeafNode {
            data: vec![],
            next: None,
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
struct InnerNode<K, V> {
    data: Vec<(NodeCell<K, V>, K)>,
    // In steady-state, inner node should never be empty, and thus should always have a non-None last
    // however for initializing and moving things around, it's handy to let it be an Option
    last: Option<NodeCell<K, V>>,
    // if we think of an inner node layed out conceptually as
    // <ptr0> (k0, v0) <ptr1> (k1, v1) ... <ptr n> <kn,vn> <last>
    // then we maintain the invariant that ALL of the keys in the whole subtree including and under ptr0 at
    // less than or equal to k0, and all the keys in the subtree including and under ptr1 are strictly greater.
    // That goes on down the line: the left child is less than or equal, the right child is stritly more.
}

impl<K, V> InnerNode<K, V>
where
    K: Ord + Copy,
    V: Copy,
{
    fn new() -> InnerNode<K, V> {
        InnerNode {
            data: vec![],
            last: None,
        }
    }

    fn leftmost_leaf(&self) -> NodeCell<K, V> {
        if !self.data.is_empty() {
            match &*self.data[0].0.borrow() {
                Leaf(_) => self.data[0].0.clone(),
                Inner(i) => i.leftmost_leaf(),
            }
        } else {
            let last = self.last.as_ref().unwrap();
            match &*last.borrow() {
                Leaf(_) => last.clone(),
                Inner(i) => i.leftmost_leaf(),
            }
        }
    }

    #[allow(dead_code)]
    fn rightmost_leaf(&self) -> NodeCell<K, V> {
        let last = self.last.as_ref().unwrap();
        match &*last.borrow() {
            Leaf(_) => last.clone(),
            Inner(i) => i.rightmost_leaf(),
        }
    }

    // This is a convience method of creating an inner node from pre-assembled data
    // No invariants are checked here, and it may panic if the `children.len()` isn't exactly one more than `pairs.len()`
    #[allow(dead_code)]
    fn from_raw(mut keys: Vec<K>, mut children: Vec<Node<K, V>>) -> InnerNode<K, V> {
        let mut data = vec![];
        let last = Some(Rc::new(RefCell::new(children.pop().unwrap())));
        while !keys.is_empty() {
            data.insert(
                0,
                (
                    Rc::new(RefCell::new(children.pop().unwrap())),
                    keys.pop().unwrap(),
                ),
            );
        }
        InnerNode { data, last }
    }

    fn get_child_by_idx(&mut self, idx: usize) -> &NodeCell<K, V> {
        if self.data.len() > idx {
            &self.data[idx].0
        } else {
            self.last.as_ref().unwrap()
        }
    }

    // Rebalances the subtree under `self` assuming that all invariants hold EXCEPT possibly for `self`'s child at index `idx`.
    // If self.get_child_by_idx(idx) has less than order keys, a rebalancing will be performed:
    // If self.get_child_by_idx(idx) is NOT self.last, we first try to pull the smallest element from self.get_child_by_idx(idx+1).
    // This will succeed if self.get_child_by_idx(idx+1) has a least order+1 keys.
    // When pulling that element, if the child is an inner node, the entire cell (ptr, (key, value)) from self.get_child_by_idx(idx+1),
    // Then we do a shuffle, putting (ptr, (key,value)) in to self.get_child_by_idx(idx), and, since that key
    // is greater than node.data[idx], setting node.data[idx] equal to that key.
    // If idx is last or there are not enough elements in the right sibling, we do the same procedure on the left, pulling the largest element.
    // When moving an element from a left child to a right child, we replace our node's data with the the largest
    // key remaning on the left child to hold up the invariant.
    // If neither of those work, we merge self.get_child_by_idx(idx) with its right sibling (if idx isn't last) or its left sibling (if it is).
    fn rebalance(&mut self, order: usize, idx: usize) {
        // During insert, it is always clear by the return value of insert_into clear whether or not we need to call rebalance.
        // But for remove, it's not clear, and requires to see if the removal forces us to rebalance.
        // For convenience (and as a safety-check), we'll verify here.
        let len = match &*self.get_child_by_idx(idx).borrow() {
            Leaf(l) => l.data.len(),
            Inner(l) => l.data.len(),
        };
        if len >= order {
            return;
        }

        if idx <= self.data.len() {
            // There is *some* child at idx+1; let's try to get it
            let my_new_key = match &mut *self.get_child_by_idx(idx + 1).clone().borrow_mut() {
                Leaf(right) if right.data.len() > order => {
                    let pair = right.data.remove(0);
                    let mut left_ref = self.get_child_by_idx(idx).borrow_mut();
                    let left = left_ref.as_leaf_mut();
                    left.data.push(pair);
                    Some(pair.0)
                }
                Inner(right) if right.data.len() > order => {
                    let (ptr, key) = right.data.remove(0);
                    let mut left_ref = self.get_child_by_idx(idx).borrow_mut();
                    let left = left_ref.as_inner_mut();
                    let new_cell = (left.last.as_ref().unwrap().clone(), key);
                    left.last = Some(ptr);
                    left.data.push(new_cell);
                    Some(key)
                }
                _ => None,
            };
            if let Some(p) = my_new_key {
                self.data[idx].1 = p;
                return;
            }
        }

        if idx > 0 {
            // There is *some* child at idx-1; let's try to get it
            let my_new_key = match &mut *self.get_child_by_idx(idx - 1).clone().borrow_mut() {
                Leaf(left) if left.data.len() > order => {
                    let pair = left.data.pop().unwrap();
                    let mut right_ref = self.get_child_by_idx(idx).borrow_mut();
                    let right = right_ref.as_leaf_mut();
                    right.data.insert(0, pair);
                    Some(left.data.last().unwrap().0)
                }
                Inner(left) if left.data.len() > order => {
                    let (new_last, pair) = left.data.pop().unwrap();
                    let ptr = left.last.clone().unwrap();
                    left.last = Some(new_last);
                    let mut right_ref = self.get_child_by_idx(idx).borrow_mut();
                    let right = right_ref.as_inner_mut();
                    let new_cell = (ptr, pair);
                    right.data.insert(0, new_cell);
                    Some(left.data.last().unwrap().1)
                }
                _ => None,
            };
            if let Some(p) = my_new_key {
                self.data[idx - 1].1 = p;
                return;
            }
        }

        // Neither right nor left sibling worked to pull a donation from, so we need to merge.
        // Always merge our deficient node with its right sibling, unless it's the last.
        if idx < self.data.len() {
            self.merge_from_right(idx);
        } else {
            // rather than implement merge_from_left, we can just say that merge_from_left(i) is the same as merge_from_right(i+1)
            self.merge_from_right(idx - 1);
        }
    }

    // if the children are leaves, just move over all the data and drop the pair from self
    // if the children are inners, we need to rotate the the (ptr, key) pairs over
    fn merge_from_right(&mut self, idx: usize) {
        let right_child = if idx < self.data.len() - 1 {
            self.data[idx + 1].0.clone()
        } else {
            self.last.as_ref().unwrap().clone()
        };
        match &mut *right_child.borrow_mut() {
            Leaf(leaf) => {
                let pairs = leaf.data.drain(0..leaf.data.len());
                let mut target_child = self.data[idx].0.borrow_mut();
                if let Leaf(left_leaf) = &mut *target_child {
                    left_leaf.data.extend(pairs);
                    left_leaf.next = leaf.next.clone();
                } else {
                    unreachable!("right child leaf, left child inner");
                }
                drop(target_child);
                let (p, _) = self.data.remove(idx);
                if idx == self.data.len() {
                    self.last = Some(p);
                } else {
                    self.data[idx].0 = p;
                }
            }
            Inner(inner) => {
                let mut keys = vec![self.data[idx].1];
                let mut ptrs = vec![];

                for (ptr, key) in inner.data.drain(0..inner.data.len()) {
                    keys.insert(0, key);
                    ptrs.insert(0, ptr);
                }
                ptrs.push(inner.last.as_ref().unwrap().clone());
                while let Some(pair) = keys.pop() {
                    let ptr = ptrs.pop().unwrap();
                    let mut target_child_ref = self.data[idx].0.borrow_mut();
                    let target_child = target_child_ref.as_inner_mut();
                    let current_last = target_child.last.as_ref().unwrap().clone();
                    target_child.data.push((current_last, pair));
                    target_child.last = Some(ptr);
                }
            }
        };
    }
}

impl<K, V> Node<K, V>
where
    K: Ord + Copy,
    V: Copy,
{
    // convience method to save some lines, especially to make writing tests more compact.
    #[allow(dead_code)]
    fn leaf_from(data: Vec<(K, V)>) -> Node<K, V> {
        Leaf(LeafNode { data, next: None })
    }

    fn len(&self) -> usize {
        match self {
            Inner(inner) => inner.data.len(),
            Leaf(leaf) => leaf.data.len(),
        }
    }

    #[allow(dead_code)]
    fn as_leaf(&self) -> &LeafNode<K, V> {
        match &self {
            Leaf(l) => l,
            _ => panic!(),
        }
    }

    fn as_leaf_mut(&mut self) -> &mut LeafNode<K, V> {
        match self {
            Leaf(l) => l,
            _ => panic!(),
        }
    }

    fn as_inner_mut(&mut self) -> &mut InnerNode<K, V> {
        match self {
            Inner(i) => i,
            _ => panic!(),
        }
    }

    // Inserts the pair `(key, value)` into `node`.
    //
    // Following the B+-Tree algorithm, we insert (k,v) by doing a search down the tree find the the single leaf in which it is supposed to go. This is implemented by having Inner nodes recursively call insert_into on their children.
    // Eventually, calling insert_into on a leaf places the new element in the leaf. However, if leaf is now too big, it may need to be split into two smaller leaves.
    // Since there is a new leaf, there needs to be an additional element in the parent of that leaf to separate the new leaf from the old leaf.
    // Note that with B+-Tree, a leaf splits when it gets to 2*order+1, and we split with the left node having order+1 and the right node having order elements.
    // That's the way that's compabitible with our convention that during traversal, comparing equal means go left.
    // That new element may cause the parent to split, and so on.
    // That way that a split is represented is the funky return type here.
    // If `node.insert_into()` causes `node` to split. This function will return `Some`. Otherwise, it will return `None`.
    // The `Some` holds the single `(key,value)` pair evicted (if from inner) or copied (if from leaf) from this subtree as the newly made right node.
    // the left node is still the same object (so that references to it do not need to be updated), it is split in-place.
    // The returned nodes will be the same `Node` variant as `node`.
    // The caller is expected to accept the evicted pair and update its own pointers to use the new  right.
    // If any children of node needed be to be split, that will be handled by this function.
    fn insert_into(&mut self, order: usize, key: K, value: V) -> Option<(K, NodeCell<K, V>)> {
        let insert_result = match self {
            Leaf(leaf) => {
                match leaf.data.binary_search_by_key(&key, |&(k, _)| k) {
                    Ok(idx) => {
                        leaf.data[idx] = (key, value);
                    }
                    Err(idx) => {
                        leaf.data.insert(idx, (key, value));
                    }
                }

                if leaf.data.len() <= 2 * order {
                    None
                } else {
                    let mut right = LeafNode::new();
                    right.next = leaf.next.as_ref().cloned();
                    let middle = leaf.data[order];
                    for (k, v) in leaf.data.drain((order + 1)..) {
                        right.data.push((k, v));
                    }
                    let right = Rc::new(RefCell::new(Leaf(right)));
                    leaf.next = Some(right.clone());
                    Some((middle.0, right))
                }
            }
            Inner(inner) => {
                // step 1: find the child to insert into
                // step 2: recursively insert into that child (which may recurse further)
                // step 3: if child split as a result, handle it,
                let first_larger = inner
                    .data
                    .iter_mut()
                    .enumerate()
                    .find(|(_, (_, k))| k > &key);
                let next_node = match first_larger {
                    None => match &mut inner.last {
                        None => panic!("inner node had empty last"),
                        Some(l) => l.clone(),
                    },
                    Some((_, (child, _))) => child.clone(),
                };
                let insert_result = next_node.borrow_mut().insert_into(order, key, value);
                match insert_result {
                    None => None,
                    Some((k, right)) => {
                        let new_key_idx: Option<usize> = first_larger.map(|(i, _)| i);
                        match new_key_idx {
                            None => {
                                let no_longer_last = inner.last.replace(right.clone());
                                inner.data.push((no_longer_last.unwrap(), k));
                                inner.last = Some(right);
                            }
                            Some(idx) => {
                                let current_kv = inner.data[idx].1;
                                inner.data[idx].1 = k;
                                inner.data.insert(idx + 1, (right, current_kv));
                            }
                        }

                        if inner.data.len() <= 2 * order {
                            None
                        } else {
                            // we need to split
                            let mut new_right = InnerNode::new();
                            for x in inner.data.drain(order + 2..2 * order + 1) {
                                new_right.data.push(x);
                            }
                            let middle = inner.data.pop().unwrap();
                            new_right.last = inner.last.clone();
                            inner.last = Some(middle.0);
                            return Some((middle.1, Rc::new(RefCell::new(Inner(new_right)))));
                        }
                    }
                }
            }
        };
        insert_result
    }

    fn get(&self, key: &K) -> Option<V> {
        match self {
            Leaf(leaf) => match leaf.data.binary_search_by(|(k, _)| k.cmp(key)) {
                Ok(idx) => Some(leaf.data[idx].1),
                Err(_) => None,
            },
            Inner(inner) => {
                let idx = match inner.data.binary_search_by_key(key, |&(_, k)| k) {
                    Ok(idx) => idx,
                    Err(idx) => idx,
                };
                if idx < inner.data.len() {
                    let (p, _) = &inner.data[idx];
                    return p.borrow().get(key);
                } else {
                    return inner.last.as_ref().map(|l| l.borrow().get(key)).flatten();
                }
            }
        }
    }

    // as with insert_into, remove may cause a node to be unbalanced; it is the parent's responsibility to fix it.
    // However, this time the type of unbalanced is simpler: it's unbalanced if it has fewer than order keys.
    // That's easy for the parent to check, so we don't need any kind of funky return type to convey it.
    fn remove(&mut self, order: usize, key: &K) -> Option<V> {
        match self {
            Leaf(leaf) => match leaf.data.binary_search_by(|(k, _)| k.cmp(key)) {
                Ok(idx) => {
                    let (_, v) = leaf.data.remove(idx);
                    Some(v)
                }
                Err(_) => None,
            },
            Inner(inner) => {
                let idx = match inner.data.binary_search_by(|(_, k)| k.cmp(key)) {
                    Ok(idx) => idx,
                    Err(idx) => idx,
                };
                let next_node = if inner.data.len() > idx {
                    inner.data[idx].0.clone()
                } else {
                    inner.last.clone().unwrap()
                };
                let mut next_target = next_node.borrow_mut();
                let return_value = next_target.remove(order, key);
                if next_target.len() < order {
                    drop(next_target); // release the mut borrow
                    inner.rebalance(order, idx);
                }
                return_value
            }
        }
    }
}

/// Extremely quick hack of a display implementation for Trees
/// The root is on the top line as a list [key1,key2,...]
/// Then the children of root are on the next line, and so on
/// You have to sorta count to figure out what's going on.
impl<K, V> Display for Tree<K, V>
where
    K: Display,
    V: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut acc: Vec<Vec<String>> = vec![];
        let mut queue: VecDeque<(usize, _)> = vec![(0, self.root.clone())].into();
        while let Some((i, node)) = queue.pop_front() {
            let kv_str: Vec<String> = match &*node.borrow() {
                Leaf(leaf) => leaf
                    .data
                    .iter()
                    .map(|(k, v)| format!("({}, {})", k, v))
                    .collect(),
                Inner(inner) => inner.data.iter().map(|(_, k)| format!("{}", k)).collect(),
            };
            let inner_str = kv_str.join(", ");
            let node_str = String::from("[") + &inner_str + "]";
            if acc.len() <= i {
                acc.resize(i + 1, vec![]);
            }
            acc[i].push(node_str);
            if let Inner(inner) = &*node.borrow() {
                for (c, _) in inner.data.iter() {
                    queue.push_back((i + 1, c.clone()));
                }
                let back_child = inner.last.clone().unwrap();
                queue.push_back((i + 1, back_child));
            }
        }
        write!(
            f,
            "{}",
            acc.iter()
                .map(|row| row.join(" , "))
                .collect::<Vec<String>>()
                .join("\n")
        )
    }
}

impl<K, V> BTree<K, V> for Tree<K, V>
where
    K: Ord + Copy,
    V: Copy,
{
    fn insert(&mut self, key: K, value: V) {
        let result = self.root.borrow_mut().insert_into(self.order, key, value);
        match result {
            None => {}
            Some((k, r)) => {
                self.root = Rc::new(RefCell::new(Inner(InnerNode {
                    data: vec![(self.root.clone(), k)],
                    last: Some(r),
                })))
            }
        }
    }

    fn get(&self, key: &K) -> Option<V> {
        self.root.borrow().get(key)
    }

    fn remove(&mut self, key: &K) -> Option<V> {
        let ret = self.root.borrow_mut().remove(self.order, key);
        if self.root.borrow().len() == 0 {
            let mut new_root = None;
            if let Inner(inner) = &*self.root.borrow() {
                new_root = Some(inner.last.as_ref().unwrap().clone());
            }
            if let Some(rc) = new_root {
                self.root = rc;
            }
        }
        ret
    }
}

pub struct Iter<K, V> {
    leaf: NodeCell<K, V>,
    idx: usize,
}

impl<K, V> Iterator for Iter<K, V>
where
    K: Copy,
    V: Copy,
{
    type Item = (K, V);

    fn next(&mut self) -> Option<Self::Item> {
        // If we're inside a leaf, advance the index and short-circuit return the value
        // If we're at the end of a leaf, reset the index and self.leaf, then try again by returning self.next()
        let next_leaf = match &*self.leaf.borrow() {
            Leaf(leaf) if self.idx < leaf.data.len() => {
                // we're not at the last element of the leaf
                self.idx += 1;
                return Some(leaf.data[self.idx - 1]);
            }
            Leaf(leaf) => {
                // we ARE at the last element of the leaf
                let t = leaf.next.clone();
                match t {
                    Some(v) => {
                        self.idx = 0;
                        v
                    }
                    None => {
                        return None;
                    }
                }
            }
            _ => unreachable!(),
        };
        self.leaf = next_leaf;
        self.next()
    }
}

impl<K, V> std::iter::FusedIterator for Iter<K, V>
where
    K: Copy,
    V: Copy,
{
}

#[cfg(test)]
mod tests {
    use std::convert::TryInto;

    use super::*;

    fn validate_tree(
        tree: &Tree<i32, i32>,
        expected_elements: &[(i32, i32)],
    ) -> Result<(), String> {
        for (k, v) in expected_elements {
            if tree.get(k) != Some(*v) {
                return Err(format!(
                    "get({}) returned {:?} instead of Some({})",
                    k,
                    tree.get(k),
                    v
                ));
            }
        }
        for (i, (got_pair, expected_pair)) in tree.iter().zip(expected_elements.iter()).enumerate()
        {
            if got_pair != *expected_pair {
                return Err(format!(
                    "element # '{}' of iter returned {:?} instead of {:?}",
                    i, got_pair, *expected_pair,
                ));
            }
        }
        Ok(())
    }

    #[test]
    fn tree_basics() {
        let mut tree: Tree<i32, i32> = Tree::new(1);
        tree.insert(1, 2);
        tree.insert(2, 3);
        tree.insert(3, 4);
        tree.insert(4, 5);
        tree.insert(5, 6);
        tree.insert(6, 7);
        tree.insert(7, 8);
        for i in 1..=7 {
            assert_eq!(tree.get(&i), Some(&i + 1));
        }
        validate_tree(
            &tree,
            &(1_i32..=7_i32).map(|x| (x, x + 1)).collect::<Vec<_>>(),
        )
        .unwrap();
    }

    #[test]
    fn iter() {
        let mut tree: Tree<i32, i32> = Tree::new(1);
        tree.insert(1, 2);
        tree.insert(2, 3);
        tree.insert(3, 4);
        tree.insert(4, 5);
        tree.insert(5, 6);
        tree.insert(6, 7);
        tree.insert(7, 8);
        let it = tree.iter();
        for (i, (k, v)) in it.enumerate() {
            let j: i32 = i.try_into().unwrap();
            assert_eq!(j + 1, k);
            assert_eq!(j + 2, v);
        }

        let mut tree2: Tree<usize, usize> = Tree::new(4);
        let cap = 1000;
        let v: Vec<_> = (0..cap).collect();
        for i in v {
            tree2.insert((7 * i) % cap, (7 * i) % cap);
        }
        println!("{}", tree2);
        for (i, (k, v)) in tree2.iter().enumerate() {
            assert_eq!(i, k);
            assert_eq!(i, v);
        }
    }

    #[test]
    fn tree_remove() {
        let mut pop_from_right_tree = Tree {
            order: 2,
            root: Rc::new(RefCell::new(Inner(InnerNode::from_raw(
                vec![2, 4],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(3, 3), (4, 4)]),
                    Node::leaf_from(vec![(5, 5), (6, 6), (7, 7)]),
                ],
            )))),
        };

        let pop_from_right_result = Tree {
            order: 2,
            root: Rc::new(RefCell::new(Inner(InnerNode::from_raw(
                vec![2, 5],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(3, 3), (5, 5)]),
                    Node::leaf_from(vec![(6, 6), (7, 7)]),
                ],
            )))),
        };

        println!("{}", pop_from_right_tree);
        pop_from_right_tree.remove(&4);
        println!("{}", pop_from_right_tree);
        assert_eq!(pop_from_right_tree.get(&4), None);
        assert_eq!(pop_from_right_tree, pop_from_right_result);
        validate_tree(
            &pop_from_right_tree,
            &vec![1, 2, 3, 5, 6, 7]
                .into_iter()
                .map(|x| (x, x))
                .collect::<Vec<_>>(),
        )
        .unwrap();

        let mut pop_from_left_tree = Tree {
            order: 2,
            root: Rc::new(RefCell::new(Inner(InnerNode::from_raw(
                vec![2, 4],
                vec![
                    Node::leaf_from(vec![(0, 0), (1, 1), (2, 2)]),
                    Node::leaf_from(vec![(3, 3), (4, 4)]),
                    Node::leaf_from(vec![(5, 5), (6, 6)]),
                ],
            )))),
        };

        let pop_from_left_result = Tree {
            order: 2,
            root: Rc::new(RefCell::new(Inner(InnerNode::from_raw(
                vec![1, 4],
                vec![
                    Node::leaf_from(vec![(0, 0), (1, 1)]),
                    Node::leaf_from(vec![(2, 2), (4, 4)]),
                    Node::leaf_from(vec![(5, 5), (6, 6)]),
                ],
            )))),
        };

        println!("{}", pop_from_left_tree);
        pop_from_left_tree.remove(&3);
        println!("{}", pop_from_left_tree);
        assert_eq!(pop_from_left_tree.get(&3), None);
        assert_eq!(pop_from_left_tree, pop_from_left_result);
        validate_tree(
            &pop_from_left_tree,
            &vec![0, 1, 2, 4, 5, 6]
                .into_iter()
                .map(|x| (x, x))
                .collect::<Vec<_>>(),
        )
        .unwrap();

        let mut pop_from_right_tree = Tree {
            order: 2,
            root: Rc::new(RefCell::new(Inner(InnerNode::from_raw(
                vec![2, 4],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(3, 3), (4, 4)]),
                    Node::leaf_from(vec![(5, 5), (6, 6), (7, 7)]),
                ],
            )))),
        };

        let pop_from_right_result = Tree {
            order: 2,
            root: Rc::new(RefCell::new(Inner(InnerNode::from_raw(
                vec![2, 4],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(3, 3), (4, 4)]),
                    Node::leaf_from(vec![(6, 6), (7, 7)]),
                ],
            )))),
        };

        println!("{}", pop_from_right_tree);
        pop_from_right_tree.remove(&5);
        println!("{}", pop_from_right_tree);
        assert_eq!(pop_from_right_tree.get(&5), None);
        assert_eq!(pop_from_right_tree, pop_from_right_result);

        let mut pop_from_left_tree = Tree {
            order: 2,
            root: Rc::new(RefCell::new(Inner(InnerNode::from_raw(
                vec![2, 4],
                vec![
                    Node::leaf_from(vec![(0, 0), (1, 1), (2, 2)]),
                    Node::leaf_from(vec![(3, 3), (4, 4)]),
                    Node::leaf_from(vec![(5, 5), (6, 6), (7, 7)]),
                ],
            )))),
        };

        let pop_from_left_result = Tree {
            order: 2,
            root: Rc::new(RefCell::new(Inner(InnerNode::from_raw(
                vec![2, 4],
                vec![
                    Node::leaf_from(vec![(0, 0), (1, 1)]),
                    Node::leaf_from(vec![(3, 3), (4, 4)]),
                    Node::leaf_from(vec![(5, 5), (6, 6), (7, 7)]),
                ],
            )))),
        };

        println!("{}", pop_from_left_tree);
        pop_from_left_tree.remove(&2);
        println!("{}", pop_from_left_tree);
        assert_eq!(pop_from_left_tree.get(&2), None);
        assert_eq!(pop_from_left_tree, pop_from_left_result);

        let mut merge_tree = Tree {
            order: 2,
            root: Rc::new(RefCell::new(Inner(InnerNode::from_raw(
                vec![2, 4],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(3, 3), (4, 4)]),
                    Node::leaf_from(vec![(5, 5), (6, 6)]),
                ],
            )))),
        };

        let merge_result = Tree {
            order: 2,
            root: Rc::new(RefCell::new(Inner(InnerNode::from_raw(
                vec![2],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(3, 3), (4, 4), (6, 6)]),
                ],
            )))),
        };

        println!("{}", merge_tree);
        merge_tree.remove(&5);
        println!("{}", merge_tree);
        assert_eq!(merge_tree.get(&5), None);
        assert_eq!(merge_tree, merge_result);

        let mut merge_tree = Tree {
            order: 2,
            root: Rc::new(RefCell::new(Inner(InnerNode::from_raw(
                vec![2, 4],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(3, 3), (4, 4)]),
                    Node::leaf_from(vec![(5, 5), (6, 6)]),
                ],
            )))),
        };

        let merge_result = Tree {
            order: 2,
            root: Rc::new(RefCell::new(Inner(InnerNode::from_raw(
                vec![2],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(3, 3), (5, 5), (6, 6)]),
                ],
            )))),
        };
        println!("{}", merge_tree);
        merge_tree.remove(&4);
        println!("{}", merge_tree);
        assert_eq!(merge_tree.get(&4), None);
        assert_eq!(merge_tree, merge_result);
    }

    #[test]
    #[ignore]
    fn mem_leak_check() {
        // We have a bunch of RCs so it's good to make sure we don't have a cycle.
        // Running with heaptrack and checking mem usage over time seems to indicate that we are okay.
        // e.g. $ cargo test --no-run
        // then $ heaptrack ./targets/<etc> --ignored bplus
        for _ in 0..10000 {
            let mut tree: Tree<usize, usize> = Tree::new(4);
            let cap = 1000;
            let v: Vec<_> = (0..cap).collect();
            for i in v {
                tree.insert((7 * i) % cap, (7 * i) % cap);
            }
        }
    }
}
