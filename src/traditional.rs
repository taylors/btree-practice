//! This module provides a naive implementation of traditional Knuth-style B-Trees.
//! Thus:
//! - Each tree has a fixed even number 2*order for the maximum keys in a node.
//! - Each node has at most that many keys, and every node except the root has at least order.
//! - Leaf nodes and interior nodes both hold (key, value) pairs.
//! The naivete here is that the memory layout is not fully optimized to achieve the CPU cache and/or disk read
//! benefits that B-Trees are capable of. Here I am more focused on the functionality.
//! Furthermore, for simplicity of implementation, I assume that keys and values are Copy to ease moving the key-value pairs around; this could be weakened to clone, however.
//! This module was mostly a warm-up exercise before implementing B-Plus, so there may be some minor issues.
#![allow(dead_code)]
#![allow(clippy::all)]

use super::btree::BTree;
use std::collections::VecDeque;
use std::fmt::Display;
use std::rc::Rc;

/// Tree is a naive B-Tree of some fixed order.
///
/// It implements the B-Tree common interface using a standard B-Tree setup as described in the module docs.
/// The main purpose of the Tree class (instead of just using the root) is that the root of a tree has special behavior.
/// For example, the root is the only node that's allowed to have less than `order` keys.
/// Thus, our strategy for implementing BTree is to implement methods on the Node class, but have the Node methods return
/// the information that a parent of that node might need.
/// For example, `insert` on a node might cause that node to split. The type of the Node's method will return information about the way in which the split occured.
/// If that node is not the root, that node's parent will be responsible for some logic to handle the split.
/// If that node IS the root, then that means the caller of `insert` is a Tree, and Tree will provide the logic to split the root.
/// And similarly for remove.
///
/// Both here and inside of Node, Rc serves as a Box with a little wiggle room to be passed around.
/// When doing rotations, it is convenient to momentarily have multiple owners of a child node until we can get the rotation finished.
/// During the steady-state (i.e. when not in the middle of a Tree method), we expected each Node to have only one owner: its parent.
#[derive(Debug, Eq, PartialEq)]
pub struct Tree<K, V> {
    root: Rc<Node<K, V>>,
    order: usize,
}

use Node::*;

impl<K, V> Tree<K, V>
where
    K: Ord,
{
    fn new(order: usize) -> Tree<K, V> {
        Tree {
            root: Rc::new(Leaf(LeafNode::new())),
            order,
        }
    }
}

// The `order` parameter could be brought into the Node type to simplify its method signatures (but duplicate the order all over).
// Doesn't really matter either way.
// The leaf variant is really simple and might not need to have its data extracted into a whole type (LeafNode),
// but InnerNode is a bit more complicated and it's more convenient to be able to give it methods.
#[derive(Debug, Eq, PartialEq)]
enum Node<K, V> {
    Inner(InnerNode<K, V>),
    Leaf(LeafNode<K, V>),
}

#[derive(Debug, Eq, PartialEq)]
struct LeafNode<K, V> {
    data: Vec<(K, V)>,
}

impl<K, V> LeafNode<K, V>
where
    K: Ord,
{
    fn new() -> LeafNode<K, V> {
        LeafNode { data: vec![] }
    }
}

#[derive(Debug, Eq, PartialEq)]
struct InnerNode<K, V> {
    data: Vec<(Rc<Node<K, V>>, (K, V))>,
    // In steady-state, inner node should never be empty, and thus should always have a non-None last
    // however for initializing and moving things around, it's handy to let it be an Option
    last: Option<Rc<Node<K, V>>>,
}

impl<K, V> InnerNode<K, V>
where
    K: Ord + Copy,
    V: Copy,
{
    fn new() -> InnerNode<K, V> {
        InnerNode {
            data: vec![],
            last: None,
        }
    }

    // This is a convience method of creating an inner node from pre-assembled data
    // No invariants are checked here, and it may panic if the `children.len()` isn't exactly one more than `pairs.len()`
    fn from_raw(mut pairs: Vec<(K, V)>, mut children: Vec<Node<K, V>>) -> InnerNode<K, V> {
        let mut data = vec![];
        let last = Some(Rc::new(children.pop().unwrap()));
        while !pairs.is_empty() {
            data.insert(0, (Rc::new(children.pop().unwrap()), pairs.pop().unwrap()));
        }
        InnerNode { data, last }
    }

    fn get_child_by_idx(&mut self, idx: usize) -> &Rc<Node<K, V>> {
        if self.data.len() > idx {
            return &self.data[idx].0;
        } else {
            return self.last.as_ref().unwrap();
        }
    }

    // Rebalances the subtree under `self` assuming that all invariants hold EXCEPT possibly for `self`'s child at index `idx`.
    // If self.get_child_by_idx(idx) has less than order keys, a rebalancing will be performed:
    // If self.get_child_by_idx(idx) is NOT self.last, we first try to pull the smallest element from self.get_child_by_idx(idx+1).
    // This will succeed if self.get_child_by_idx(idx+1) has a least order+1 keys.
    // When pulling that element, if the child is an inner node, the entire cell (ptr, (key, value)) from self.get_child_by_idx(idx+1),
    // Then we do a shuffle, putting (key,value) into node.data[idx], and putting ptr plus what used to be in node.data[idx] into self.get_child_by_idx(idx)
    // If idx is last or there are not enough elements in the right sibling, we do the same procedure on the left, pulling the largest element.
    // If neither of those work, we merge self.get_child_by_idx(idx) with its right sibling (if idx isn't last) or its left sibling (if it is).
    fn rebalance(&mut self, order: usize, idx: usize) {
        // During insert, it is always by the return value of insert_into clear whether or not we need to call rebalance.
        // But for remove, it's not clear, and requires to see if the removal forces us to rebalance.
        // For convenience (and as a safety-check), we'll verify here.
        let len = match self.get_child_by_idx(idx).as_ref() {
            Leaf(l) => l.data.len(),
            Inner(l) => l.data.len(),
        };
        if len >= order {
            return;
        }

        let donation = self.try_pop_from_right(order, idx);
        if let Some((p, (k, v))) = donation {
            // If pop from right worked, we know idx is not last. Thus idx < self.data.len(), and this indexing is safe
            let (rc_child, (new_k, new_v)) = &mut self.data[idx];
            let child = Rc::get_mut(rc_child).unwrap();
            match child {
                Leaf(leaf) => {
                    leaf.data.push((*new_k, *new_v));
                }
                Inner(inner) => {
                    let new_cell = (inner.last.as_ref().unwrap().clone(), (*new_k, *new_v));
                    inner.last = Some(p.unwrap());
                    inner.data.push(new_cell);
                }
            }
            self.data[idx].1 = (k, v);
            return;
        }

        let donation = self.try_pop_from_left(order, idx);
        if let Some((p, (k, v))) = donation {
            let (new_k, new_v) = self.data[idx - 1].1;
            let rc_child = &mut self.data[idx].0;
            let child = Rc::get_mut(rc_child).unwrap();
            match child {
                Leaf(leaf) => {
                    leaf.data.insert(0, (new_k, new_v));
                }
                Inner(inner) => {
                    inner.data.insert(0, (p.unwrap(), (new_k, new_v)));
                }
            }
            self.data[idx - 1].1 = (k, v);
            return;
        }

        // Neither right nor left sibling worked to pull a donation from, so we need to merge.
        // Always merge our deficient node with its right sibling, unless it's the last.
        if idx < self.data.len() {
            self.merge_from_right(idx);
        } else {
            // rather than implement merge_from_left, we can just say that merge_from_left(i) is the same as merge_from_right(i+1)
            self.merge_from_right(idx - 1);
        }
    }

    fn merge_from_right(&mut self, idx: usize) {
        let mut pairs = vec![self.data[idx].1];
        let mut ptrs = vec![];
        while let Some((ptr, pair)) = self.pop_from_right(idx) {
            pairs.insert(0, pair);
            ptrs.insert(0, ptr);
        }
        let right_child = if idx < self.data.len() - 1 {
            self.data[idx + 1].0.clone()
        } else {
            self.last.as_ref().unwrap().clone()
        };
        match right_child.as_ref() {
            Leaf(_) => ptrs.push(None),
            Inner(inner) => ptrs.push(Some(inner.last.as_ref().unwrap().clone())),
        }
        while let Some(pair) = pairs.pop() {
            let ptr = ptrs.pop().unwrap();
            let target_child = Rc::get_mut(&mut self.data[idx].0).unwrap();
            match target_child {
                Leaf(leaf) => {
                    leaf.data.push(pair);
                }
                Inner(inner) => {
                    let current_last = inner.last.as_ref().unwrap().clone();
                    inner.data.push((current_last, pair));
                    inner.last = ptr;
                }
            }
        }
        let (p, _) = self.data.remove(idx);
        if idx == self.data.len() {
            self.last = Some(p);
        } else {
            self.data[idx].0 = p;
        }
    }

    // try_pop_from_right is a helper method for rebalancing self.data[idx] (or self.last if idx==self.data.len())
    // It tries to pull the first cell (i.e (key, value) pair, plus, if the children are inner nodes, the corresponding child pointer)
    // from the sibling one-to-the-right of idx.
    // It fails if idx is last (there is no right sibling) or if the right sibling has exactly order elements; then it returns None.
    // If it succeed, but the children are leaf nodes (and thus don't have any pointers), returns Some((None, (key, value)))
    // If it succeeds and the children are inner nodes, the first element of the tuple is the pointer pulled from the right sibling.
    fn try_pop_from_right(
        &mut self,
        order: usize,
        idx: usize,
    ) -> Option<(Option<Rc<Node<K, V>>>, (K, V))> {
        if idx == self.data.len() {
            // would correspond to last
            return None;
        }
        let child = if idx + 1 == self.data.len() {
            self.last.as_mut().unwrap()
        } else {
            &mut self.data[idx + 1].0
        };

        match Rc::get_mut(child).unwrap() {
            Leaf(leaf) => {
                if leaf.data.len() <= order {
                    return None;
                }
                let pair = leaf.data.remove(0);
                return Some((None, pair));
            }
            Inner(inner) => {
                if inner.data.len() <= order {
                    return None;
                }
                let (p, pair) = inner.data.remove(0);
                return Some((Some(p), pair));
            }
        };
    }

    // see try_pop_from_right. This pulls the last cell from the left sibling, instead.
    fn try_pop_from_left(
        &mut self,
        order: usize,
        idx: usize,
    ) -> Option<(Option<Rc<Node<K, V>>>, (K, V))> {
        if idx == 0 {
            return None;
        }
        let child = &mut self.data[idx - 1].0;
        match Rc::get_mut(child).unwrap() {
            Leaf(leaf) => {
                if leaf.data.len() <= order {
                    return None;
                }
                let pair = leaf.data.pop();
                return Some((None, pair.unwrap()));
            }
            Inner(inner) => {
                if inner.data.len() <= order {
                    return None;
                }
                let (p, pair) = inner.data.pop().unwrap();
                let ptr = inner.last.clone();
                inner.last = Some(p);
                return Some((ptr, pair));
            }
        };
    }

    // pop_from_right is a helper method for merge_from_right.
    // In the case of merging, we do actually want to drain the right sibling entirely.
    // So, it is nearly the same as try_pop_from_right, but it does not fail if the right sibling
    // has order or fewer elements. It fails if idx is last (so that there is no right sibling) or if
    // the right sibling has 0 elements.
    fn pop_from_right(&mut self, idx: usize) -> Option<(Option<Rc<Node<K, V>>>, (K, V))> {
        if idx == self.data.len() {
            // would correspond to last
            return None;
        }
        let child = if idx + 1 == self.data.len() {
            self.last.as_mut().unwrap()
        } else {
            &mut self.data[idx + 1].0
        };

        match Rc::get_mut(child).unwrap() {
            Leaf(leaf) => {
                if leaf.data.len() == 0 {
                    return None;
                }
                let pair = leaf.data.remove(0);
                return Some((None, pair));
            }
            Inner(inner) => {
                if inner.data.len() == 0 {
                    return None;
                }
                let (p, pair) = inner.data.remove(0);
                return Some((Some(p), pair));
            }
        };
    }
}

impl<K, V> Node<K, V>
where
    K: Ord + Copy,
    V: Copy,
{
    // convience method to save some lines, especially to make writing tests more compact.
    fn leaf_from(data: Vec<(K, V)>) -> Node<K, V> {
        Leaf(LeafNode { data })
    }

    fn len(&self) -> usize {
        match self {
            Inner(inner) => inner.data.len(),
            Leaf(leaf) => leaf.data.len(),
        }
    }

    // Inserts the pair `(key, value)` into `node`.
    //
    // Following the B-Tree algorithm, we insert (k,v) by doing a search down the tree find the the single leaf in which it is supposed to go. This is implemented by having Inner nodes recursively call insert_into on their children.
    // Eventually, calling insert_into on a leaf places the new element in the leaf. However, if leaf is now too big, it may need to be split into two smaller leaves and push one element from the leaf up into the parent.
    // That new element may cause the parent to split, and so on.
    // That way that a split is represented is the funky return type here.
    // If `node.insert_into()` causes `node` to split. This function will return `Some`. Otherwise, it will return `None`.
    // The `Some` holds the single `(key,value)` pair evicted from this subtree as well two nodes, the new left and
    // new right subtrees. The returned nodes will be the same `Node` variant as `node`.
    // The caller is expected to accept the evicted pair and update its own pointers to use the new left and right.
    // If any children of node needed be to be split, that will be handled by this function.
    fn insert_into(
        &mut self,
        order: usize,
        key: K,
        value: V,
    ) -> Option<((K, V), Node<K, V>, Node<K, V>)> {
        let insert_result = match self {
            Leaf(leaf) => {
                match leaf.data.binary_search_by_key(&key, |&(k, _)| k) {
                    Ok(idx) => {
                        leaf.data[idx] = (key, value);
                    }
                    Err(idx) => {
                        leaf.data.insert(idx, (key, value));
                    }
                }

                if leaf.data.len() <= 2 * order {
                    None
                } else {
                    let mut left = LeafNode::new();
                    let mut right = LeafNode::new();
                    let mut middle = None;
                    for (i, (k, v)) in leaf.data.iter().enumerate() {
                        if i < order {
                            left.data.push((*k, *v))
                        } else if i > order {
                            right.data.push((*k, *v))
                        } else {
                            middle = Some((*k, *v))
                        }
                    }
                    Some((middle.unwrap(), Leaf(left), Leaf(right)))
                }
            }
            Inner(inner) => {
                // step 1: find the child to insert into
                // step 2: recursively insert into that child (which may recurse further)
                // step 3: if child split as a result, handle it,
                let first_larger = inner
                    .data
                    .iter_mut()
                    .enumerate()
                    .find(|(_, (_, (k, _)))| k > &key);
                // idx here is the index that next_node will be moved to if the recursive insert causes a split
                let (idx, next_node): (Option<usize>, &mut Node<K, V>) = match first_larger {
                    None => match &mut inner.last {
                        None => panic!("inner node had empty last"),
                        Some(l) => (None, Rc::get_mut(l).unwrap()),
                    },
                    Some((i, (child, _))) => (Some(i), Rc::get_mut(child).unwrap()),
                };
                match next_node.insert_into(order, key, value) {
                    None => None,
                    Some(((k, v), left, right)) => {
                        // for covenience of implementation, we'll always insert, allowing our data array to potentially overflow
                        // then make another pass to split if necessary
                        inner.data.push((Rc::from(left), (k, v)));

                        // idx tells us whether we are inserting into inner.last (idx == None)
                        // or else what the index into inner.data of the cell just to the right of the new entry
                        match idx {
                            Some(idx) => {
                                inner.data[idx].0 = Rc::from(right);
                                inner
                                    .data
                                    .sort_unstable_by(|(_, (k1, _)), (_, (k2, _))| k1.cmp(k2));
                            }
                            None => {
                                inner.last = Some(Rc::from(right));
                            }
                        }
                        if inner.data.len() < 2 * order {
                            None
                        } else {
                            // we need to split
                            let mut new_left = InnerNode::new();
                            let mut new_right = InnerNode::new();
                            let mut middle = None;
                            for (i, (left_child, (k, v))) in inner.data.iter().enumerate() {
                                if i < order {
                                    new_left.data.push((left_child.clone(), (*k, *v)));
                                } else if i > order {
                                    new_right.data.push((left_child.clone(), (*k, *v)));
                                } else {
                                    new_left.last = Some(left_child.clone());
                                    middle = Some((*k, *v))
                                }
                            }
                            new_right.last = inner.last.as_ref().map(|x| x.clone());
                            Some((middle.unwrap(), Inner(new_left), Inner(new_right)))
                        }
                    }
                }
            }
        };
        insert_result
    }

    fn get(&self, key: &K) -> Option<V> {
        match self {
            Leaf(leaf) => {
                for (k, v) in &leaf.data {
                    if k == key {
                        return Some(*v);
                    } else if key < k {
                        return None;
                    }
                }
                None
            }
            Inner(inner) => {
                for (p, (k, v)) in &inner.data {
                    if k == key {
                        return Some(*v);
                    } else if key < k {
                        return p.get(key);
                    }
                }
                return inner.last.as_ref().map(|l| l.get(key)).flatten();
            }
        }
    }

    // as with insert_into, remove may cause a node to be unbalanced; it is the parent's responsibility to fix it.
    // However, this time the type of unbalanced is simpler: it's unbalanced if it has fewer than order keys.
    // That's easy for the parent to check, so we don't need any kind of funky return type to convey it.
    fn remove(&mut self, order: usize, key: &K) -> Option<V> {
        match self {
            Leaf(leaf) => match leaf.data.binary_search_by(|(k, _)| k.cmp(key)) {
                Ok(idx) => {
                    let (_, v) = leaf.data.remove(idx);
                    return Some(v);
                }
                Err(_) => {
                    return None;
                }
            },
            Inner(inner) => match inner.data.binary_search_by(|(_, (k, _v))| k.cmp(key)) {
                Ok(idx) => {
                    // we are removing from an inner node: we need to promote an element from a subtree to fill the void
                    // We first check the child to the right of the removed node, pulling out its first cell if it has strictly more than order keys.
                    // that can only fail if the removed key is the last key .. TODO wait can it fail?
                    // If so, check the left.
                    // The tree rotation that occurs is similar to the story with try_pop_from_* in insert.
                    let new_pair = if inner.data.len() > idx + 1 {
                        let donating_subtree = Rc::get_mut(&mut inner.data[idx + 1].0).unwrap();
                        let pair = donating_subtree.pop_largest_from(order);
                        if let Inner(donating_inner) = donating_subtree {
                            donating_inner.rebalance(order, donating_inner.data.len());
                        }
                        pair
                    } else {
                        let donating_subtree = Rc::get_mut(inner.last.as_mut().unwrap()).unwrap();
                        let pair = donating_subtree.pop_smallest_from(order);
                        if let Inner(donating_inner) = donating_subtree {
                            donating_inner.rebalance(order, 0);
                        }
                        pair
                    };
                    let v = (inner.data[idx].1).1;
                    inner.data[idx].1 = new_pair;
                    inner.rebalance(order, idx + 1);
                    return Some(v);
                }
                Err(idx) => {
                    // key isn't present in this inner node, but idx tells us which child node to recurse into
                    let next_target = if inner.data.len() > idx {
                        Rc::get_mut(&mut inner.data[idx].0).unwrap()
                    } else {
                        Rc::get_mut(inner.last.as_mut().unwrap()).unwrap()
                    };
                    let return_value = next_target.remove(order, key);
                    if next_target.len() < order {
                        inner.rebalance(order, idx);
                    }
                    return return_value;
                }
            },
        }
    }

    // pop_smallest_from removes the smallest element from the subtree at self.
    // After calling on a node, its entire subtree will be balanced except possibly the node itself.
    fn pop_smallest_from(&mut self, order: usize) -> (K, V) {
        match self {
            Leaf(leaf) => leaf.data.remove(0),
            Inner(inner) => {
                let pair = Rc::get_mut(&mut inner.data[0].0)
                    .unwrap()
                    .pop_smallest_from(order);
                inner.rebalance(order, 0);
                pair
            }
        }
    }

    // pop_largest_from removes the largest element from the subtree at self.
    // After calling on a node, its entire subtree will be balanced except possibly the node itself.
    fn pop_largest_from(&mut self, order: usize) -> (K, V) {
        match self {
            Leaf(leaf) => leaf.data.pop().unwrap(),
            Inner(inner) => {
                let pair = Rc::get_mut(&mut inner.last.as_mut().unwrap())
                    .unwrap()
                    .pop_largest_from(order);
                inner.rebalance(order, inner.data.len());
                pair
            }
        }
    }
}

/// Extremely quick hack of a display implementation for Trees
/// The root is on the top line as a list [key1,key2,...]
/// Then the children of root are on the next line, and so on
/// You have to sorta count to figure out what's going on.
impl<K, V> Display for Tree<K, V>
where
    K: Display,
    V: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut acc: Vec<Vec<String>> = vec![];
        let mut queue: VecDeque<(usize, &Node<K, V>)> = vec![(0, self.root.as_ref())].into();
        while let Some((i, node)) = queue.pop_front() {
            let kv_str: Vec<String> = match &node {
                Leaf(leaf) => leaf
                    .data
                    .iter()
                    .map(|(k, v)| format!("({}, {})", k, v))
                    .collect(),
                Inner(inner) => inner
                    .data
                    .iter()
                    .map(|(_, (k, v))| format!("({}, {})", k, v))
                    .collect(),
            };
            let inner_str = kv_str.join(", ");
            let node_str = String::from("[") + &inner_str + "]";
            if acc.len() <= i {
                acc.resize(i + 1, vec![]);
            }
            acc[i].push(node_str);
            if let Inner(inner) = &node {
                for (c, _) in inner.data.iter() {
                    queue.push_back((i + 1, c));
                }
                let back_child = inner.last.as_ref().unwrap().as_ref();
                queue.push_back((i + 1, back_child));
            }
        }
        write!(
            f,
            "{}",
            acc.iter()
                .map(|row| row.join(" , "))
                .collect::<Vec<String>>()
                .join("\n")
        )
    }
}

impl<K, V> BTree<K, V> for Tree<K, V>
where
    K: Ord + Copy,
    V: Copy,
{
    fn insert(&mut self, key: K, value: V) {
        let result = Rc::get_mut(&mut self.root)
            .unwrap()
            .insert_into(self.order, key, value);
        match result {
            None => {}
            Some(((k, v), l, r)) => {
                self.root = Rc::new(Inner(InnerNode {
                    data: vec![(Rc::new(l), (k, v))],
                    last: Some(Rc::new(r)),
                }))
            }
        }
    }

    fn get(&self, key: &K) -> Option<V> {
        self.root.get(key)
    }

    fn remove(&mut self, key: &K) -> Option<V> {
        let ret = Rc::get_mut(&mut self.root).unwrap().remove(self.order, key);
        if self.root.len() == 0 {
            let mut new_root = None;
            if let Inner(inner) = &self.root.as_ref() {
                new_root = Some(inner.last.as_ref().unwrap().clone());
            }
            if let Some(rc) = new_root {
                self.root = rc.clone();
            }
        }
        ret
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basics() {
        let mut node: Node<i32, i32> = Leaf(LeafNode::new());
        let result = node.insert_into(1, 1, 2);
        assert_eq!(result, None);
        if let Leaf(leaf) = &node {
            assert_eq!(leaf.data, vec![(1, 2)]);
        } else {
            panic!();
        }

        let result = node.insert_into(1, 2, 3);
        assert_eq!(result, None);
        if let Leaf(leaf) = &node {
            assert_eq!(leaf.data, vec![(1, 2), (2, 3)]);
        } else {
            panic!();
        }

        let result = node.insert_into(1, 3, 4);
        assert_ne!(result, None);
        match &result {
            Some(((k, v), Leaf(l), Leaf(r))) => {
                assert_eq!((*k, *v), (2, 3));
                assert_eq!(l.data, vec![(1, 2)]);
                assert_eq!(r.data, vec![(3, 4)]);
            }
            _ => panic!(),
        }
    }

    #[test]
    fn tree_basics() {
        let mut tree: Tree<i32, i32> = Tree::new(1);
        tree.insert(1, 2);
        tree.insert(2, 3);
        tree.insert(3, 4);
        tree.insert(4, 5);
        tree.insert(5, 6);
        tree.insert(6, 7);
        tree.insert(7, 8);
        println!("{:?}", tree);
        println!("{}", tree);
        for i in 1..=7 {
            assert_eq!(tree.get(&i), Some(&i + 1));
        }
    }

    #[test]
    fn tree_remove_from_leaf() {
        let mut pop_from_right_tree = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(3, 3), (6, 6)],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(4, 4), (5, 5)]),
                    Node::leaf_from(vec![(7, 7), (8, 8), (9, 9)]),
                ],
            ))),
        };

        let pop_from_right_result = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(3, 3), (7, 7)],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(4, 4), (6, 6)]),
                    Node::leaf_from(vec![(8, 8), (9, 9)]),
                ],
            ))),
        };

        println!("{}", pop_from_right_tree);
        pop_from_right_tree.remove(&5);
        println!("{}", pop_from_right_tree);
        assert_eq!(pop_from_right_tree.get(&5), None);
        assert_eq!(pop_from_right_tree, pop_from_right_result);

        let mut pop_from_left_tree = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(3, 3), (6, 6)],
                vec![
                    Node::leaf_from(vec![(0, 0), (1, 1), (2, 2)]),
                    Node::leaf_from(vec![(4, 4), (5, 5)]),
                    Node::leaf_from(vec![(7, 7), (8, 8)]),
                ],
            ))),
        };

        let pop_from_left_result = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(2, 2), (6, 6)],
                vec![
                    Node::leaf_from(vec![(0, 0), (1, 1)]),
                    Node::leaf_from(vec![(3, 3), (4, 4)]),
                    Node::leaf_from(vec![(7, 7), (8, 8)]),
                ],
            ))),
        };

        println!("{}", pop_from_left_tree);
        pop_from_left_tree.remove(&5);
        println!("{}", pop_from_left_tree);
        assert_eq!(pop_from_left_tree.get(&5), None);
        assert_eq!(pop_from_left_tree, pop_from_left_result);

        let mut merge_tree = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(3, 3), (6, 6)],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(4, 4), (5, 5)]),
                    Node::leaf_from(vec![(7, 7), (8, 8)]),
                ],
            ))),
        };

        let merge_result = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(3, 3)],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(4, 4), (6, 6), (7, 7), (8, 8)]),
                ],
            ))),
        };

        println!("{}", merge_tree);
        merge_tree.remove(&5);
        println!("{}", merge_tree);
        assert_eq!(merge_tree.get(&5), None);
        assert_eq!(merge_tree, merge_result);

        let mut merge_tree = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(3, 3), (6, 6)],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(4, 4), (5, 5)]),
                    Node::leaf_from(vec![(7, 7), (8, 8)]),
                ],
            ))),
        };

        let merge_result = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(3, 3)],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(4, 4), (5, 5), (6, 6), (8, 8)]),
                ],
            ))),
        };

        println!("{}", merge_tree);
        merge_tree.remove(&7);
        println!("{}", merge_tree);
        assert_eq!(merge_tree.get(&7), None);
        assert_eq!(merge_tree, merge_result);
    }

    #[test]
    fn tree_remove_from_inner() {
        let mut input = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(3, 3), (6, 6)],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(4, 4), (5, 5)]),
                    Node::leaf_from(vec![(7, 7), (8, 8), (9, 9)]),
                ],
            ))),
        };

        let output = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(3, 3), (7, 7)],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(4, 4), (5, 5)]),
                    Node::leaf_from(vec![(8, 8), (9, 9)]),
                ],
            ))),
        };

        println!("{}", input);
        input.remove(&6);
        println!("{}", input);
        assert_eq!(input.get(&6), None);
        assert_eq!(input, output);

        let mut input = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(3, 3), (6, 6)],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(4, 4), (5, 5)]),
                    Node::leaf_from(vec![(7, 7), (8, 8)]),
                ],
            ))),
        };

        let output = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(3, 3)],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(4, 4), (5, 5), (7, 7), (8, 8)]),
                ],
            ))),
        };

        println!("{}", input);
        input.remove(&6);
        println!("{}", input);
        assert_eq!(input.get(&6), None);
        assert_eq!(input, output);

        let mut input = Tree {
            order: 2,
            root: Rc::new(Inner(InnerNode::from_raw(
                vec![(3, 3)],
                vec![
                    Node::leaf_from(vec![(1, 1), (2, 2)]),
                    Node::leaf_from(vec![(4, 4), (5, 5)]),
                ],
            ))),
        };

        let output = Tree {
            order: 2,
            root: Rc::new(Node::leaf_from(vec![(1, 1), (2, 2), (4, 4), (5, 5)])),
        };

        println!("{}", input);
        input.remove(&3);
        println!("{}", input);
        assert_eq!(input.get(&3), None);
        assert_eq!(input, output);
    }
}
