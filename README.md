# btree-practice

This is a hobby repository in which I am practicing and refining the implementation of a few variants of B-Trees. This code is not suitable for any use anywhere. You may find it interesting to peruse if you are curious about implementing B-Trees in Rust. Or not.

I am mostly following the combination of Wikipedia, the 1979 survey paper "The Ubiquitous B-Tree" by Douglas Comar, and my own pencil-and-paper work to fill in the gaps.

So far, I have implemented what I'm calling a "traditional B-Tree" as well as B+-trees; see the respective module docs for more info.

TODO
- B+ Tree with lazy loading and unloading of nodes
- B+ Tree with ^ and max key count based on storage size, variable length keys
